# Carousel javascript css only  

![Simple carousel](./carousel.png)

#### This project was made as a part of my new blog.  

## A responsive Javascript only carousel, that slides multiple items with effect.
### Mainly also made to make it accessible to screenreaders.
### Responsive by default.

# Initialization 
    " If you move the init from calculator.js to html file. "
     const carousels = document.querySelectorAll('[data-carousel]');
     carousels.forEach(carousel => new Carousel(carousel));

# Responsive
    The javascript engine takes care of the responsive layout. 
    When the screen changes, the built in breakpoints are activated.
    For eg. a 4 slide become 3 -> 2 -> 1 dependent on screen change.
    The onload event sets the slides in viewport to the maximum of the screensize.  

# Customize items per slide
    Number of items per slide.
    Change the number variable [data-num-per-slide].
    For eg. data-num-per-slide="3" will display 3 items in the viewport.
    Up to 5 (20% width), if more needed, add it to the css. There's no limitation, just add the corresponding class.
    If none is provided, 1 single slide is shown.

# Changing design.
    To make changes to the content, edit .slide-content class.
    There are som examples on how to change the layout in the div of .slide-content class.
