/*
* Carousel class that allows multiple instances of a carousel on a page. 
* Todo: Move to React JS code.
*/
class Carousel {
    /*
    * constructor: takes a carousel [object] as parameter.
    */
    constructor(carousel, window) {
     
      this.window = window;
      /*
      * The carousel object */
      this.carousel = carousel;
      /*
      * Next and previous buttons */
      this.buttonPrevious = carousel.querySelector('[data-carousel-button-previous]');
      this.buttonNext = carousel.querySelector('[data-carousel-button-next]');
      
      /*
      * The actual container for slide elements. */
      this.slidesContainer = carousel.querySelector('[data-carousel-slides-container]');

      /*
      * Number of slides per slide. Default value is 1*/
      this.numPerSlide = this.slidesContainer.getAttribute('data-num-per-slide') || 1;
      this.savedState = this.numPerSlide;
      /*
      * Add css class to each slide*/
      Array.from(this.slidesContainer.children).forEach(child=> child.classList.add(`slide-${this.numPerSlide}`))


  
      /*
      * Total amount of slides.*/
      this.numSlides = this.slidesContainer.children.length;
      this.currentSlide = 0;
      
      /*
      * Eventhandlers */
      this.buttonPrevious.addEventListener('click', this.handlePrevious.bind(this));
      this.buttonNext.addEventListener('click', this.handleNext.bind(this));
      this.window.addEventListener('load', this.handleWindowChange.bind(this));
      this.window.addEventListener('resize', this.handleWindowChange.bind(this));
    }
  
    /*
    * Handles the next slide */
    handleNext() {
      console.log('Count ', this.numPerSlide)
      this.currentSlide = this.nextNumber(this.currentSlide + 1, this.numSlides, this.numPerSlide);
      this.carousel.style.setProperty('--current-slide', this.currentSlide);
    }
    
    /*
    * Handles the previous slide */
    handlePrevious() {
      this.currentSlide = this.nextNumber(this.currentSlide - 1, this.numSlides, this.numPerSlide);
      this.carousel.style.setProperty('--current-slide', this.currentSlide);
    }

    /*
    * Handles window load/resize changes 
    * The onload sets the correct amount of slides inside viewport. */
    handleWindowChange(){
      var windowWidth = this.window.innerWidth;
      var slides = this.numPerSlide;

      /*
      * Breakpoints 
      * Handles the breakpoints from 5 slides, down to 1 */
      const breakpoints = {
        1: {breakAt: 400, breakTo: 1},
        2: {breakAt: 500, breakTo: 1},
        3: {breakAt: 600, breakTo: 2},
        4: {breakAt: 700, breakTo: 3},
        5: {breakAt: 800, breakTo: 4}
      }

      if(windowWidth < breakpoints[slides].breakAt){
        this.numPerSlide = breakpoints[slides].breakTo;
        this.slidesContainer.setAttribute('data-num-per-slide', breakpoints[slides].breakTo);
        
        Array.from(this.slidesContainer.children).forEach(child=> { 
          child.classList.remove(`slide-${slides}`); 
          child.classList.add(`slide-${breakpoints[slides].breakTo}`)});
      }else{
        this.numPerSlide = this.savedState; //restore state
        this.slidesContainer.setAttribute('data-num-per-slide', slides);
        
        Array.from(this.slidesContainer.children).forEach(child=> {
          child.classList.remove(`slide-${breakpoints[slides].breakTo}`); 

          if(this.savedState === breakpoints[slides].breakTo)
            child.classList.add(`slide-${this.savedState}`);
          else
            child.classList.add(`slide-${slides}`);
        });
      }
    }
  
    /*
    * Function to calulate next number.
    * 
    * @param number: The current slide, integer
    * @param mod: Total amount of slides, integer
    * @param numPerSlide: How many slides to show in viewport, integer
    */
    nextNumber(number, mod, numPerSlide) {
      let result = number % Math.ceil(mod/numPerSlide);
      if (result < 0) {
        result += Math.ceil(mod/numPerSlide);
      }
      return result;
    }
  }
  
  const carousels = document.querySelectorAll('[data-carousel]');
  carousels.forEach(carouselElement => new Carousel(carouselElement, window));